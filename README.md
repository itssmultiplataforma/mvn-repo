# Maven private repository#

Para utilizar este repositório, adicione no pom:

```
#!xml
<repositories>
	<repository>
		<id>itsstecnologia-repo</id>
		<name>Repository for ITSS private libs</name>
		<url>https://bitbucket.org/itssmultiplataforma/mvn-repo/raw/master/repository/</url>
	</repository>
</repositories>
```

## Dependências disponíveis##
### PRETTY-TOOLS-JDDE ###

```
#!xml
<dependency>
	<groupId>pretty-tools-JDDE</groupId>
	<artifactId>pretty-tools-JDDE</artifactId>
	<version>2.0.3</version>
</dependency>
```
### SAP-JCO ###

```
#!xml
<dependency>
	<groupId>com.sap</groupId>
	<artifactId>sap-jco</artifactId>
	<version>3.0.9</version>
</dependency>
```
### ITSS Arquitetura ###

```
#!xml
<dependency>
	<groupId>br.com.itss.arquitetura</groupId>
	<artifactId>itss-commons</artifactId>
	<version>1.0.0</version>
</dependency>

<dependency>
	<groupId>br.com.itss.arquitetura</groupId>
	<artifactId>itss-modelo</artifactId>
	<version>1.0.0</version>
</dependency>

<dependency>
	<groupId>br.com.itss.arquitetura</groupId>
	<artifactId>itss-persistencia</artifactId>
	<version>1.0.0</version>
</dependency>
```